'use strict'

const genericStart = require('../generic')
const config = require('../../config')
const { createItinerary } = require('../../services/itinerary')

const createItineraryStart = () => {
  genericStart({
    queueName: config.messaging.create_itinerary.queue.name,
    queueTtl: config.messaging.create_itinerary.queue.ttl,
    exchange: config.messaging.create_itinerary.exchange.name,
    exchangeRoutingKey: config.messaging.create_itinerary.exchange.routing_key,
    deadletterExchange: config.messaging.create_itinerary.exchange.dead_letter_name,
    deadletterRoutingKey: config.messaging.create_itinerary.exchange.dead_letter_routing_key
  }, createItinerary('Create a new Itinerary'))
}

module.exports = createItineraryStart