'use strict';

const amqp = require('amqplib');
const config = require('../../config');
const logger = require('../../commons/lib/logger')(config);

const ERR_INVALID_JSON = 'Invalid JSON.';
const TEN_MINUTES_IN_MS = '600000';
const ONE_HOUR_IN_MS = '3600000';

const queueOpts = (ttl, deadletterExchange, deadletterRoutingKey) => ({
  durable: true,
  autoDelete: false,
  arguments: {
    'x-message-ttl': parseInt(ttl, 10),
    'x-dead-letter-exchange': deadletterExchange,
    'x-dead-letter-routing-key': deadletterRoutingKey,
  },
});

const tryParseJson = jsonString => new Promise(resolve => resolve(JSON.parse(jsonString)));

const isFirstFail = message => !message.failCount;

const isSecondFail = message => message.failCount === 1;

const handleError = ({ channel, queueArgs }, parsedMessage) => {
  if (isFirstFail(parsedMessage)) {
    parsedMessage.failCount = 1;

    const tenMinutesDelayQueue = `${queueArgs.queueName}-delay-10`;

    channel.assertQueue(tenMinutesDelayQueue, queueOpts(
      TEN_MINUTES_IN_MS, // TTL: 10 minutes
      queueArgs.exchange, // after TTL delay, resend to original queue exchange
      queueArgs.exchangeRoutingKey,
    ))
      .then(() => {
        logger.warn('Sending to 10 delay queue.');
        channel.sendToQueue(tenMinutesDelayQueue, Buffer.from(JSON.stringify(parsedMessage)));
      });
  } else if (isSecondFail(parsedMessage)) {
    parsedMessage.failCount++;

    const oneHourDelayQueue = `${queueArgs.queueName}-delay-60`;
    channel.assertQueue(oneHourDelayQueue, queueOpts(
      ONE_HOUR_IN_MS, // TTL: 60 minutes
      queueArgs.exchange, // after TTL delay, resend to original queue exchange
      queueArgs.exchangeRoutingKey,
    ))
      .then(() => {
        logger.warn('Sending to 60 delay queue.');
        channel.sendToQueue(oneHourDelayQueue, Buffer.from(JSON.stringify(parsedMessage)));
      });
  } else {
    logger.warn('Third try failed, sending to DLQ.');


    const deadLetter = `${queueArgs.queueName}_deadletter`;
    channel.assertQueue(deadLetter)
      .then(() => {
        logger.warn('Sending to DLQ queue.');
        channel.sendToQueue(deadLetter, Buffer.from(JSON.stringify(parsedMessage)));
      });
  }
};

const handleMessage = (channel, { queueArgs, handler }) => (msg) => {
  const logError = (errorMsg, err) => {
    const message = Object.assign({}, msg);
    message.content = message.content.toString();
    logger.error({ err, message }, errorMsg);
    return channel.nack(msg, false, false);
  };

  tryParseJson(msg.content.toString())
    .then((parsedObject) => {
      handler(parsedObject)
        .catch(() => {
          handleError({ channel, queueArgs }, parsedObject);
        });
    })
    .catch(err => logError(ERR_INVALID_JSON, err));
};

const genericStart = (queueArgs, handler) => {
  amqp.connect(config.rabbit.url)
    .then((conn) => {
      conn.createChannel()
        .then((ch) => {
          ch.assertQueue(
            queueArgs.queueName,
            queueOpts(queueArgs.queueTtl, queueArgs.deadletterExchange, queueArgs.deadletterRoutingKey),
          )
            .then(() => {
              logger.debug(`${queueArgs.queueName} consumer started.`);
              return ch.consume(queueArgs.queueName, handleMessage(ch, { queueArgs, handler }), { noAck: true });
            })
            .catch((err) => {
              logger.error({ err }, `Fail to assert queue ${queueArgs.queueName}.`);
            });
        })
        .catch((err) => {
          logger.error({ err }, 'Fail to create channel.');
        });
    })
    .catch((err) => {
      logger.error({ err }, 'Fail to connect in RabbitMQ server.');
    });
};

module.exports = genericStart;
